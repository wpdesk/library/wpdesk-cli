<?php

declare(strict_types=1);

namespace WPDesk\Console\Git;

use Psr\Log\LoggerInterface;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class BleGitCommit
{
    public function __construct(
        private readonly iterable $files,
        private readonly LoggerInterface $logger
    ) {
    }

    public function commit(string $commitMessage): void
    {
        ( new Process([ 'git', 'add', ...$this->files ]) )->run();

        $commit = new Process(['git', 'commit', '-m', $commitMessage, '-n' ]);
        $commit->run();

        if (!$commit->isSuccessful()) {
            $restore = new Process(['git', 'restore', '--staged', '--worktree', ...$this->files]);
            $restore->run();

            $this->logger->error('Failed to commit changes');
            throw new ProcessFailedException($commit);
        }
    }
}

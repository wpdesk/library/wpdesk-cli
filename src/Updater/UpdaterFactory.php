<?php

declare(strict_types=1);

namespace WPDesk\Console\Updater;

use WPDesk\Console\Updater\Metadata\ProgramEnum;
use WPDesk\Console\Updater\Metadata\Version;

class UpdaterFactory
{
    public function fromVersion(Version $version): Updater
    {
        return match ($version->program) {
            ProgramEnum::WordPress => new WordPressUpdater($version->version),
            ProgramEnum::WooCommerce => new WooCommerceUpdater($version->version),
            ProgramEnum::PHP => new PHPUpdater($version->version),
            ProgramEnum::Plugin => new PluginUpdater($version->version)
        };
    }
}

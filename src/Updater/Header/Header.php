<?php declare( strict_types=1 );

namespace WPDesk\Console\Updater\Header;

interface Header
{

    public function change(string $targetVersion): array;

}

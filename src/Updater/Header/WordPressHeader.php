<?php

declare(strict_types=1);

namespace WPDesk\Console\Updater\Header;

use Composer\Pcre\Preg;
use Symfony\Component\Finder\Finder;

use function file_put_contents;

class WordPressHeader implements Header
{
    public function change(string $targetVersion): array
    {
        $finder = new Finder();
        $finder->files()
            ->in(getcwd())
            ->exclude(['vendor', 'vendor_prefixed'])
            ->name(['*.php', 'readme.txt'])
            ->contains('Tested up to:');

        $changedFiles = [];
        foreach ($finder as $file) {
            $original = $file->getContents();
            $content = Preg::replace(
                '/Tested up to: [\d.]+/',
                'Tested up to: ' . $targetVersion,
                $original
            );
            if ($content !== $original) {
                $changedFiles[] = $file->getRelativePathname();
                file_put_contents($file->getRealPath(), $content);
            }
        }
        return $changedFiles;
    }
}

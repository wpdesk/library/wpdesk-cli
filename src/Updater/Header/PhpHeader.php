<?php

declare(strict_types=1);

namespace WPDesk\Console\Updater\Header;

use Composer\Pcre\Preg;
use Symfony\Component\Finder\Finder;

class PhpHeader implements Header
{
    public function change(string $targetVersion): array
    {
        $finder = new Finder();
        $finder->files()
            ->in(getcwd())
            ->exclude(['vendor', 'vendor_prefixed'])
            ->name('*.php')
            ->contains('Requires PHP: ');

        $changedFiles = [];
        foreach ($finder as $file) {
            $original = $file->getContents();
            $content = Preg::replace(
                '/Requires PHP: [\d.]+/',
                'Requires PHP: ' . $targetVersion,
                $original
            );

            if ($content !== $original) {
                $changedFiles[] = $file->getRealPath();
                file_put_contents($file->getRealPath(), $content);
            }
        }

        return $changedFiles;
    }
}

<?php

declare(strict_types=1);

namespace WPDesk\Console\Updater\Header;

use Composer\Pcre\Preg;
use Symfony\Component\Finder\Finder;

class WooCommerceHeader implements Header
{
    public function change(string $targetVersion): array
    {
        $finder = new Finder();
        $finder->files()
            ->in(getcwd())
            ->exclude(['vendor', 'vendor_prefixed'])
            ->name('*.php')
            ->contains('WC tested up to:');

        $changedFiles = [];
        foreach ($finder as $file) {
            $original = $file->getContents();
            $content = Preg::replace(
                '/WC tested up to: [\d.]+/',
                'WC tested up to: ' . $targetVersion,
                $original
            );

            if ($content !== $original) {
                $changedFiles[] = $file->getRealPath();
                file_put_contents($file->getRealPath(), $content);
            }
        }

        return $changedFiles;
    }
}

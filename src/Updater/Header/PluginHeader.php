<?php

declare(strict_types=1);

namespace WPDesk\Console\Updater\Header;

use Composer\Pcre\Preg;
use Symfony\Component\Finder\Finder;

class PluginHeader implements Header
{
    public function change(string $targetVersion): array
    {
        $finder = new Finder();
        $finder->files()
            ->in(getcwd())
            ->depth('< 1')
            ->name('*.php')
            ->contains('Version: ');

        $changedFiles = [];
        foreach ($finder as $file) {
            $original = $file->getContents();
            $content = Preg::replace(
                '/Version: [\d.]+/',
                'Version: ' . $targetVersion,
                $original
            );

            if ($content !== $original) {
                $changedFiles[] = $file->getRealPath();
                file_put_contents($file->getRealPath(), $content);
            }
        }

        return $changedFiles;
    }
}

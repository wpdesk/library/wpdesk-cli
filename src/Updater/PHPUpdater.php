<?php

declare(strict_types=1);

namespace WPDesk\Console\Updater;

use WPDesk\Console\Updater\Header\Header;
use WPDesk\Console\Updater\Header\PhpHeader;

class PHPUpdater implements Updater
{
    public function __construct(
        private readonly ?string $version,
        private readonly Header $changeHeader = new PhpHeader()
    ) {
        if ($this->version === null) {
            throw new \InvalidArgumentException('You must specify version to update.');
        }
    }

    public function getTargetVersion(): string
    {
        return $this->version;
    }

    public function bumpVersion(): array
    {
        return $this->changeHeader->change($this->version);
    }
}

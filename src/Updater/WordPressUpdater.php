<?php

declare(strict_types=1);

namespace WPDesk\Console\Updater;

use GuzzleHttp\Client;
use WPDesk\Console\Updater\Header\Header;
use WPDesk\Console\Updater\Header\WordPressHeader;

class WordPressUpdater implements Updater
{
    public function __construct(
        private ?string $version = null,
        private readonly Client $client = new Client(),
        private readonly Header $changeHeader = new WordPressHeader()
    ) {
        if ($this->version === null) {
            $this->version = $this->getLatestVersion();
        }
    }

    private function getLatestVersion(): string
    {
        $response = $this->client->get('https://api.wordpress.org/core/version-check/1.7/');
        $data = json_decode($response->getBody()->getContents(), true);
        return $data['offers'][0]['version'];
    }

    public function getTargetVersion(): string
    {
        return $this->version;
    }

    public function bumpVersion(): array
    {
        return $this->changeHeader->change($this->version);
    }
}

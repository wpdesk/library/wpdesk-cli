<?php

declare(strict_types=1);

namespace WPDesk\Console\Updater;

use WPDesk\Console\Updater\Header\PluginHeader;
use WPDesk\Console\Updater\Header\PluginVariableHeader;
use WPDesk\Console\Updater\Header\ReadmeHeader;

class PluginUpdater implements Updater
{
    public function __construct(
        private ?string $version = null,
        private readonly iterable $headers = [
            new PluginHeader(),
            new ReadmeHeader(),
            new PluginVariableHeader()
        ]
    ) {
        if ($this->version === null) {
            throw new \InvalidArgumentException('You must specify version to update.');
        }
    }

    public function getTargetVersion(): string
    {
        return $this->version;
    }

    public function bumpVersion(): array
    {
        $changedFiles = [];
        foreach ($this->headers as $header) {
            $changedFiles = $header->change($this->version);
        }
        return $changedFiles;
    }
}

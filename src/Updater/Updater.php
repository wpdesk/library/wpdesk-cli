<?php

declare(strict_types=1);

namespace WPDesk\Console\Updater;

interface Updater
{
    /**
     * @return string[] Array of changed files.
     */
    public function bumpVersion(): array;

    public function getTargetVersion(): string;
}

<?php

declare(strict_types=1);

namespace WPDesk\Console\Updater\Metadata;

enum ProgramEnum {
case WordPress;
case WooCommerce;
case PHP;
case Plugin;

    public static function fromString(string $program): ProgramEnum
    {
        return match ($program) {
            'wordpress', 'wp' => self::WordPress,
            'woocommerce', 'wc' => self::WooCommerce,
            'php' => self::PHP,
            'self' => self::Plugin,
            default => throw new \Exception('Unknown program'),
        };
    }
    }

<?php declare( strict_types=1 );

namespace WPDesk\Console\Updater\Metadata;

class Version
{

    public function __construct(
        public readonly ProgramEnum $program,
        public readonly ?string $version = null
    ) {
    }

}

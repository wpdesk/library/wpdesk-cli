<?php

declare(strict_types=1);

namespace WPDesk\Console\Updater;

use GuzzleHttp\Client;
use WPDesk\Console\Updater\Header\Header;
use WPDesk\Console\Updater\Header\WooCommerceHeader;

class WooCommerceUpdater implements Updater
{
    public function __construct(
        private ?string $version = null,
        private readonly Client $client = new Client(),
        private readonly Header $changeHeader = new WooCommerceHeader()
    ) {
        if ($this->version === null) {
            $this->version = $this->getLatestVersion();
        }
    }

    private function getLatestVersion(): string
    {
        $response = $this->client->get('https://api.wordpress.org/plugins/info/1.0/woocommerce.json');
        $data = json_decode($response->getBody()->getContents(), true);
        return $data['version'];
    }

    public function getTargetVersion(): string
    {
        return $this->version;
    }

    public function bumpVersion(): array
    {
        return $this->changeHeader->change($this->version);
    }
}

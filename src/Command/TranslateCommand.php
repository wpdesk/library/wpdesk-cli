<?php

declare(strict_types=1);

namespace WPDesk\Console\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;
use WP_CLI\I18n\MakePotCommand;
use WPDesk\Console\Translation\Metadata\PackageTranslation;
use WPDesk\Console\Translation\TranslationCreator;

#[AsCommand(
    name: 'translate',
    description: 'Creates .pot translation for plugin',
)]
class TranslateCommand extends Command
{
    protected function configure(): void
    {
        $this->addArgument('source', InputArgument::OPTIONAL, default: getcwd());
    }

    private function removeOldFiles(string $translationsPath)
    {
        $oldFilesFinder = new Finder();

        $oldFilesFinder
            ->in($translationsPath)
            ->name(['*.po', '*.mo', '*.json'])
            ->notName('/^\w+\.po/');

        foreach ($oldFilesFinder as $file) {
            unlink($file->getRealPath());
        }
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $packageTranslation = PackageTranslation::fromFile($input->getArgument('source') . '/composer.json');
        if (!$packageTranslation->isTranslatable()) {
            $output->write('<error>Could not find text domain</error>');
            return Command::FAILURE;
        }

        $this->removeOldFiles($packageTranslation->getTranslationsPath());

        $this->replaceVendorDomain($packageTranslation->getTextDomain());

        $this->generatePot($input->getArgument('source'));

        $translator = new TranslationCreator($packageTranslation);

        foreach ($packageTranslation->getTargetTranslations() as $poFile) {
            $translator->translate($poFile);
        }

        return Command::SUCCESS;
    }

    private function generatePot(string $source)
    {
        $makePot = new MakePotCommand();
        $makePot([$source], []);
    }

    private function replaceVendorDomain(string $textDomain): void
    {
        $finder = new Finder();
        $finder->files()->in(getcwd() . '/vendor_prefixed')->name('composer.json');

        foreach ($finder as $file) {
            $vendorPackage = PackageTranslation::fromFile($file->getRealPath());
            if (!$vendorPackage->isTranslatable()) {
                continue;
            }

            $replaceFinder = new Finder();
            $replaceFinder
                ->files()
                ->name('*.php')
                ->in($file->getPath());

            foreach ($replaceFinder as $toReplace) {
                $contents = $toReplace->getContents();
                // @todo: handle replacement more precisely - if anything resembles text domain, then it's replaced
                $result = str_replace($vendorPackage->getTextDomain(), $textDomain, $contents);
                file_put_contents($toReplace->getRealPath(), $result);
            }
        }
    }
}

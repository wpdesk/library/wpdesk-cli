<?php

declare(strict_types=1);

namespace WPDesk\Console\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;
use WPDesk\Console\Git\BleGitCommit;
use WPDesk\Console\Updater\Metadata\ProgramEnum;
use WPDesk\Console\Updater\Metadata\Version;
use WPDesk\Console\Updater\UpdaterFactory;

#[AsCommand(
    name: 'bump',
    description: 'Bump WordPress version',
)]
class BumpCommand extends Command
{
    protected function configure(): void
    {
        $this->addArgument(
            'program',
            InputArgument::REQUIRED,
            'Program to bump version.'
        );
        $this->addArgument(
            'version',
            InputArgument::OPTIONAL,
            'Target version. If not set, will be bumped to latest version.'
        );

        $this->addOption(
            'no-commit',
            mode: InputOption::VALUE_NONE,
            description: 'Do not commit changes'
        );
        $this->addOption(
            'no-changelog',
            mode: InputOption::VALUE_NONE,
            description: 'Do not update changelog'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $program = ProgramEnum::fromString(strtolower($input->getArgument('program')));

        $output->writeln(sprintf('Bumping %s version', $program->name));

        if ($this->canCommitChanges($input)) {
            ( new Process([ 'git', 'stash', '--include-untracked' ]) )->run();
        }

        $factory = new UpdaterFactory();
        $versionString = $input->hasArgument('version') ? $input->getArgument('version') : null;
        $updater = $factory->fromVersion(new Version($program, $versionString));

        $changedFiles = $updater->bumpVersion();

        if (!empty($changedFiles) && $this->canCommitChanges($input)) {
            $git = new BleGitCommit($changedFiles, new ConsoleLogger($output));
            $git->commit(sprintf('chore: bump %s compatibility version to %s', $program->name, $updater->getTargetVersion()));
        }

        if ($this->canCommitChanges($input)) {
            ( new Process([ 'git', 'stash', 'pop' ]) )->run();
        }

        return Command::SUCCESS;
    }

    private function canCommitChanges(InputInterface $input): bool
    {
        return $input->getOption('no-commit') === false;
    }
}

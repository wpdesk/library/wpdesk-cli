<?php declare( strict_types=1 );

namespace WPDesk\Console\Translation\Metadata;

use Symfony\Component\Finder\Finder;
use WPDesk\Console\Translation\TranslationFileNotFound;

class PackageTranslation
{

    public static function fromFile(string $filePath): static
    {
        $package = json_decode(file_get_contents($filePath), true);
        $package['path'] = new \SplFileInfo($filePath);

        return new self($package);
    }

    public function __construct(private readonly array $package)
    {
    }

    public function getTextDomain(): ?string
    {
        return $this->package['extra']['text-domain'] ?? null;
    }

    public function isTranslatable(): bool
    {
        return isset($this->package['extra']['text-domain']);
    }

    public function getPotFile(): \SplFileInfo
    {
        return new \SplFileInfo($this->getTranslationsPath() . DIRECTORY_SEPARATOR . $this->getTextDomain() . '.pot');
    }

    public function getPoPath(string $language): string
    {
        $finder = new Finder();
        $finder->in($this->getTranslationsPath())
            ->name([$this->getTextDomain() . '-'. $language . '.po', $language . '.po', $this->package['extra']['po-files'][$language]]);

        foreach ( $finder as $foundFile ) {
            return $foundFile->getRealPath();
        }

        throw new TranslationFileNotFound();
    }

    public function getPoCompilationPath(string $language): string
    {
        return $this->getTranslationsPath() . DIRECTORY_SEPARATOR . $this->getTextDomain() . '-' . $language . '.po';
    }

    public function languageExists(string $language): bool
    {
        return isset($this->package['extra']['po-files'][$language]);
    }

    public function getTranslationsPath(): string
    {
        return implode(
            DIRECTORY_SEPARATOR, [
            $this->package['path']->getPath(),
            $this->package['extra']['translations-folder'] ?? 'lang'
            ]
        );
    }

    /**
     * @return \Generator<PoFile> 
     */
    public function getTargetTranslations(): \Generator
    {
        $files = $this->package['extra']['po-files'] ?? [];

        foreach ($files as $language => $poFile) {
            yield new PoFile($poFile, $language);
        }
    }
}

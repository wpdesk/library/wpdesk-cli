<?php declare( strict_types=1 );

namespace WPDesk\Console\Translation\Metadata;

class PoFile
{

    public function __construct(
        public readonly string $poFileName,
        public readonly string $language
    ) {
    }
}

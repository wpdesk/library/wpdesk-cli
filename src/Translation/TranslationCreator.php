<?php declare( strict_types=1 );

namespace WPDesk\Console\Translation;

use Gettext\Merge;
use Gettext\Translations;
use Symfony\Component\Finder\Finder;
use WP_CLI\I18n\MakeJsonCommand;
use WPDesk\Console\Translation\Metadata\PackageTranslation;
use WPDesk\Console\Translation\Metadata\PoFile;

class TranslationCreator
{

    public function __construct(
        private readonly PackageTranslation $mainMeta
    ) {
    }

    public function translate(PoFile $file): void
    {
        $this->createPo($file);
        $this->mergeVendor($file);
        $this->createMo($file);
        $this->createJson($file);
    }

    private function createPo(PoFile $file): void
    {
        $currentTranslation = Translations::fromPoFile($this->mainMeta->getTranslationsPath() . DIRECTORY_SEPARATOR . $file->poFileName);
        $targetTranslation = Translations::fromPoFile($this->mainMeta->getPotFile()->getRealPath());
        //        $translations = $this->createTranslationsWithoutDisabled($translations);

        $targetTranslation->mergeWith($currentTranslation, Merge::REFERENCES_OURS | Merge::ADD | Merge::TRANSLATION_OVERRIDE);
        $targetTranslation->setHeader('Language', $file->language);
        $targetTranslation->toPoFile($this->mainMeta->getPoCompilationPath($file->language));
    }

    private function mergeVendor(PoFile $poFile): void
    {
        $finder = new Finder();
        $finder->files()->in(getcwd() . '/vendor')->name('composer.json');

        foreach ($finder as $file) {
            $vendorMeta = PackageTranslation::fromFile($file->getRealPath());
            if (! $vendorMeta->isTranslatable() || ! $vendorMeta->languageExists($poFile->language)) {
                continue;
            }

            //            try {
            //                $vendorTranslation = Translations::fromPoFile($vendorMeta->getPoPathWithoutTextDomain($poFile->language));
            //            } catch (\Exception $e) {
            //            }
            $vendorTranslation = Translations::fromPoFile($vendorMeta->getPoPath($poFile->language));
            $targetTranslation = Translations::fromPoFile($this->mainMeta->getPoPath($poFile->language));
            $targetTranslation->mergeWith($vendorTranslation);
            $targetTranslation->toPoFile($this->mainMeta->getPoCompilationPath($poFile->language));
        }
    }

    private function createMo(PoFile $file): void
    {
        $t = Translations::fromPoFile($this->mainMeta->getPoPath($file->language));
        $t->toMoFile(
            $this->mainMeta->getTranslationsPath() .
            DIRECTORY_SEPARATOR .
            $this->mainMeta->getTextDomain() .
            '-' .
            $file->language .
            '.mo'
        );
    }

    private function createJson(PoFile $file)
    {
        $makeJson = new MakeJsonCommand();
        $makeJson([$this->mainMeta->getPoCompilationPath($file->language)], []);
    }
}

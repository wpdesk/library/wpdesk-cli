<?php declare( strict_types=1 );

namespace WPDesk\Console\Translation;

class TranslationFileNotFound extends \RuntimeException
{
}